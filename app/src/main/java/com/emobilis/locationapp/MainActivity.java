package com.emobilis.locationapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {
    TextView textLocation;
    //variable to store decoded location
    String address;
    //request code to match the permission
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 1;

    //converting lat and lon  to an actual address
    Geocoder geocoder;
    List<Address> addresses;

    double lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find the ref
        textLocation = findViewById(R.id.location);

        //intialize the GEOCODER class
        geocoder = new Geocoder(this, Locale.getDefault());

        //oncreate i will ask for users location in the case the feature is turned off
        turnOnLocation();
    }

    private void turnOnLocation() {
        //use location manager to get Location Service
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        // using Lm i check whether location permission is enabled by user
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            //alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //customize my alertbuilder
            builder.setTitle("Location Permission");
            builder.setMessage("Location needs to be enabled inorder for app to be used efficiently");

            //give alert options
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                     //i will call the intent to make user turn on location feature for my app
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            //negative option
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(MainActivity.this, "Location needs to be turned on. Otherwise app is useless", Toast.LENGTH_SHORT).show();
                }
            });

            //show your alert dialog
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();


        } else {
            Toast.makeText(this, "Location is actually on.", Toast.LENGTH_SHORT).show();
        }
    }


    //onclick for getting current location
    public void getCurrent(View v){
        //call the method to ask for users permission
        getLocations();
    }

    private void getLocations() {
        new AlertDialog.Builder(this)
                .setTitle("Allow me to get your current location")
                .setMessage("this will allow for better services")

                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //i check whether permission is granted

                        if (ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                            //from this point it will move to the onRequestPermissionResult method
                            ActivityCompat.requestPermissions(
                                    MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION
                            );

                        } else {
                            //if the permission is already set
                            getCurrentLocation();
                        }

                    }
                })

                //add a negative
                .setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();

    }

    //onrequestpermission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION  && grantResults.length > 0){
            getCurrentLocation();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    //get users current location
    private void getCurrentLocation(){
        //request for user location from Android OS
        final LocationRequest locationRequest = new LocationRequest(); //create instance of LocationRequest class
        //set metadata
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(MainActivity.this)
                .requestLocationUpdates(locationRequest,new LocationCallback(){
                    @Override
                     public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        //we can get lat lon , geocoder to convert this to an actual address
                        LocationServices.getFusedLocationProviderClient(MainActivity.this)
                                .removeLocationUpdates(this);
                        //check the location result
                        if (locationResult != null && locationResult.getLocations().size() > 0){

                            //set up location index which will allow us to get the location lat and longitude
                            int latestlocationIndex = locationResult.getLocations().size() - 1; //removing the last known location from our array list

                            //setting our latitude and longitude to double variables
                            lat = locationResult.getLocations().get(latestlocationIndex).getLatitude();
                            lon = locationResult.getLocations().get(latestlocationIndex).getLongitude();

                            Log.d("bill","lat is " + lat + " lon is "  + lon);

                            //use geocoder to get the actual address
                            try{
                               //populate list item with the addresses result
                               addresses = geocoder.getFromLocation(lat,lon,1); //1 reps the maximum result to be received
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //using the list i can access the details of the user current location
                            //address
                            address = addresses.get(0).getAddressLine(0);
                            //get other info about users current location
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();

                            //set the user current location  to text view
                            textLocation.setText(address);
                        }
                    }
                }, Looper.getMainLooper());
    }
}
